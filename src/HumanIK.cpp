#include "HumanIK.h"
#include <vector>
#include <PhysicsDirectSpaceState.hpp>
#include <World.hpp>
#include <PhysicsServer.hpp>
#include <Spatial.hpp>
#include <algorithm>

using namespace godot;

void HumanIK::_register_methods()
{
    register_property<HumanIK, bool>("", &HumanIK::validBody, false, GODOT_METHOD_RPC_MODE_DISABLED, GODOT_PROPERTY_USAGE_STORAGE);
    register_property<HumanIK, bool>("", &HumanIK::validLeftHand, false, GODOT_METHOD_RPC_MODE_DISABLED, GODOT_PROPERTY_USAGE_STORAGE);
    register_property<HumanIK, bool>("", &HumanIK::validRightHand, false, GODOT_METHOD_RPC_MODE_DISABLED, GODOT_PROPERTY_USAGE_STORAGE);
    register_property<HumanIK, bool>("", &HumanIK::validLeftFoot, false, GODOT_METHOD_RPC_MODE_DISABLED, GODOT_PROPERTY_USAGE_STORAGE);
    register_property<HumanIK, bool>("", &HumanIK::validRightFoot, false, GODOT_METHOD_RPC_MODE_DISABLED, GODOT_PROPERTY_USAGE_STORAGE);

    // register_property<HumanIK, bool>("", &HumanIK::headTrackerEnabled, false, GODOT_METHOD_RPC_MODE_DISABLED, GODOT_PROPERTY_USAGE_STORAGE);
    // register_property<HumanIK, bool>("", &HumanIK::leftHandTrackerEnabled, false, GODOT_METHOD_RPC_MODE_DISABLED, GODOT_PROPERTY_USAGE_STORAGE);
    // register_property<HumanIK, bool>("", &HumanIK::rightHandTrackerEnabled, false, GODOT_METHOD_RPC_MODE_DISABLED, GODOT_PROPERTY_USAGE_STORAGE);
    // register_property<HumanIK, bool>("", &HumanIK::hipTrackerEnabled, true, GODOT_METHOD_RPC_MODE_DISABLED, GODOT_PROPERTY_USAGE_STORAGE);
    // register_property<HumanIK, bool>("", &HumanIK::leftFootTrackerEnabled, true, GODOT_METHOD_RPC_MODE_DISABLED, GODOT_PROPERTY_USAGE_STORAGE);
    // register_property<HumanIK, bool>("", &HumanIK::rightFootTrackerEnabled, true, GODOT_METHOD_RPC_MODE_DISABLED, GODOT_PROPERTY_USAGE_STORAGE);

    register_property<HumanIK, Skeleton *>("", &HumanIK::skeleton, nullptr, GODOT_METHOD_RPC_MODE_DISABLED, GODOT_PROPERTY_USAGE_STORAGE);

    register_property<HumanIK, Spatial *>("", &HumanIK::headTargetSpatial, nullptr, GODOT_METHOD_RPC_MODE_DISABLED, GODOT_PROPERTY_USAGE_STORAGE);
    register_property<HumanIK, Spatial *>("", &HumanIK::handLeftTargetSpatial, nullptr, GODOT_METHOD_RPC_MODE_DISABLED, GODOT_PROPERTY_USAGE_STORAGE);
    register_property<HumanIK, Spatial *>("", &HumanIK::handRightTargetSpatial, nullptr, GODOT_METHOD_RPC_MODE_DISABLED, GODOT_PROPERTY_USAGE_STORAGE);
    register_property<HumanIK, Spatial *>("", &HumanIK::hipTargetSpatial, nullptr, GODOT_METHOD_RPC_MODE_DISABLED, GODOT_PROPERTY_USAGE_STORAGE);
    register_property<HumanIK, Spatial *>("", &HumanIK::footLeftTargetSpatial, nullptr, GODOT_METHOD_RPC_MODE_DISABLED, GODOT_PROPERTY_USAGE_STORAGE);
    register_property<HumanIK, Spatial *>("", &HumanIK::footRightTargetSpatial, nullptr, GODOT_METHOD_RPC_MODE_DISABLED, GODOT_PROPERTY_USAGE_STORAGE);

    register_property<HumanIK, NodePath>("Skeleton", &HumanIK::setSkeletonPath, &HumanIK::getSkeletonPath, NodePath(".."));

    register_property<HumanIK, Transform>("Head Target", &HumanIK::headTarget, Transform(), GODOT_METHOD_RPC_MODE_DISABLED, GODOT_PROPERTY_USAGE_STORAGE);
    register_property<HumanIK, Transform>("Left Hand Target", &HumanIK::handLeftTarget, Transform(), GODOT_METHOD_RPC_MODE_DISABLED, GODOT_PROPERTY_USAGE_STORAGE);
    register_property<HumanIK, Transform>("Right Hand Target", &HumanIK::handRightTarget, Transform(), GODOT_METHOD_RPC_MODE_DISABLED, GODOT_PROPERTY_USAGE_STORAGE);
    register_property<HumanIK, Transform>("Hip Target", &HumanIK::hipTarget, Transform(), GODOT_METHOD_RPC_MODE_DISABLED, GODOT_PROPERTY_USAGE_STORAGE);
    register_property<HumanIK, Transform>("Left Foot Target", &HumanIK::footLeftTarget, Transform(), GODOT_METHOD_RPC_MODE_DISABLED, GODOT_PROPERTY_USAGE_STORAGE);
    register_property<HumanIK, Transform>("Right Foot Target", &HumanIK::footRightTarget, Transform(), GODOT_METHOD_RPC_MODE_DISABLED, GODOT_PROPERTY_USAGE_STORAGE);

    register_property<HumanIK, NodePath>("Head Target", &HumanIK::setHeadTargetNode, &HumanIK::getHeadTargetNode, NodePath());
    register_property<HumanIK, NodePath>("Left Hand Target", &HumanIK::setHandLeftTargetNode, &HumanIK::getHandLeftTargetNode, NodePath());
    register_property<HumanIK, NodePath>("Right Hand Target", &HumanIK::setHandRightTargetNode, &HumanIK::getHandRightTargetNode, NodePath());
    register_property<HumanIK, NodePath>("Hip Target", &HumanIK::setHipTargetNode, &HumanIK::getHipTargetNode, NodePath());
    register_property<HumanIK, NodePath>("Left Foot Target", &HumanIK::setFootLeftTargetNode, &HumanIK::getFootLeftTargetNode, NodePath());
    register_property<HumanIK, NodePath>("Right Foot Target", &HumanIK::setFootRightTargetNode, &HumanIK::getFootRightTargetNode, NodePath());

    register_property<HumanIK, String>("Bone Assignments", &HumanIK::boneAssignmentsGroup, "", GODOT_METHOD_RPC_MODE_DISABLED, GODOT_PROPERTY_USAGE_GROUP);
    register_property<HumanIK, int64_t>("Head Bone Index", &HumanIK::setHeadBone, &HumanIK::getHeadBone, -1);
    register_property<HumanIK, PoolIntArray>("Neck Bone Indexes", &HumanIK::setNeckBones, &HumanIK::getNeckBones, PoolIntArray());
    register_property<HumanIK, int64_t>("Shoulder Left Bone Index", &HumanIK::setShoulderLeftBone, &HumanIK::getShoulderLeftBone, -1);
    register_property<HumanIK, int64_t>("Upper Arm Left Bone Index", &HumanIK::setUpperArmLeftBone, &HumanIK::getUpperArmLeftBone, -1);
    register_property<HumanIK, int64_t>("Forearm Left Bone Index", &HumanIK::setForearmLeftBone, &HumanIK::getForearmLeftBone, -1);
    register_property<HumanIK, int64_t>("Hand Left Bone Index", &HumanIK::setHandLeftBone, &HumanIK::getHandLeftBone, -1);
    register_property<HumanIK, int64_t>("Shoulder Right Bone Index", &HumanIK::setShoulderRightBone, &HumanIK::getShoulderRightBone, -1);
    register_property<HumanIK, int64_t>("Upper Arm Right Bone Index", &HumanIK::setUpperArmRightBone, &HumanIK::getUpperArmRightBone, -1);
    register_property<HumanIK, int64_t>("Forearm Right Bone Index", &HumanIK::setForearmRightBone, &HumanIK::getForearmRightBone, -1);
    register_property<HumanIK, int64_t>("Hand Right Bone Index", &HumanIK::setHandRightBone, &HumanIK::getHandRightBone, -1);
    register_property<HumanIK, PoolIntArray>("Spine Bone Indexes", &HumanIK::setSpineBones, &HumanIK::getSpineBones, PoolIntArray());
    register_property<HumanIK, int64_t>("Hip Bone Index", &HumanIK::setHipBone, &HumanIK::getHipBone, -1);
    register_property<HumanIK, int64_t>("Thigh Left Bone Index", &HumanIK::setThighLeftBone, &HumanIK::getThighLeftBone, -1);
    register_property<HumanIK, int64_t>("Shin Left Bone Index", &HumanIK::setShinLeftBone, &HumanIK::getShinLeftBone, -1);
    register_property<HumanIK, int64_t>("Foot Left Bone Index", &HumanIK::setFootLeftBone, &HumanIK::getFootLeftBone, -1);
    register_property<HumanIK, int64_t>("Thigh Right Bone Index", &HumanIK::setThighRightBone, &HumanIK::getThighRightBone, -1);
    register_property<HumanIK, int64_t>("Shin Right Bone Index", &HumanIK::setShinRightBone, &HumanIK::getShinRightBone, -1);
    register_property<HumanIK, int64_t>("Foot Right Bone Index", &HumanIK::setFootRightBone, &HumanIK::getFootRightBone, -1);

    register_property<HumanIK, String>("IK Adjustments", &HumanIK::IKAdjustmentsGroup, "", GODOT_METHOD_RPC_MODE_DISABLED, GODOT_PROPERTY_USAGE_GROUP);
    register_property<HumanIK, float>("Floor Height", &HumanIK::floorHeight, 0.0);
    register_property<HumanIK, float>("Dangle Looseness", &HumanIK::relaxAmount, 30.0);
    register_property<HumanIK, float>("Crouch Bend Factor", &HumanIK::crouchBendFactor, 50.0);
    register_property<HumanIK, float>("Hunch Factor", &HumanIK::hunchFactor, 0.0);
    register_property<HumanIK, float>("Shoulder Flex Multiplier", &HumanIK::shoulderFlex, 10.0);
    register_property<HumanIK, float>("Max Leg Stretch", &HumanIK::legStretch, 0.1);
    register_property<HumanIK, float>("Max Arm Stretch", &HumanIK::armStretch, 0.1);

    register_property<HumanIK, float>("Elbow Angle Offset", &HumanIK::elbowAngleOffset, -90);
    register_property<HumanIK, float>("Elbow Rest Offset", &HumanIK::elbowRestOffset, -45.0);
    register_property<HumanIK, float>("Shoulder Twisting", &HumanIK::shoulderTwist, 50);
    register_property<HumanIK, float>("Wrist Twisting", &HumanIK::wristTwist, 66.666);
    register_property<HumanIK, Vector3>("Elbow Pole Offset", &HumanIK::elbowPoleOffset, Vector3(0, 0, -60));
    register_property<HumanIK, Vector3>("Hand Position Elbow Influence", &HumanIK::elbowPositionBias, Vector3(-100, 0, -50.0));
    register_property<HumanIK, float>("Hand Rotation Elbow Influence", &HumanIK::elbowRotationBias, 50.0);
    register_property<HumanIK, float>("Knee Angle Offset", &HumanIK::kneeAngleOffset, 180.0);
    register_property<HumanIK, float>("Knee Rest Offset", &HumanIK::kneeRestOffset, 0.0);
    register_property<HumanIK, float>("Hip Twisting", &HumanIK::hipTwist, 0);
    register_property<HumanIK, float>("Ankle Twisting", &HumanIK::ankleTwist, 20);
    register_property<HumanIK, Vector3>("Knee Pole Offset", &HumanIK::kneePoleOffset, Vector3(-30, -20, 0));
    register_property<HumanIK, Vector3>("Foot Position Knee Influence", &HumanIK::kneePositionBias, Vector3(0, 0, 30));
    register_property<HumanIK, float>("Foot Rotation Knee Influence", &HumanIK::kneeRotationBias, 10.0);

    register_property<HumanIK, Vector2>("Spine Curve Axis", &HumanIK::curveAxis, Vector2(-1, 0));
    register_property<HumanIK, float>("Hip Turn Speed", &HumanIK::hipTurnSpeed, 5.0);
    register_property<HumanIK, float>("Spine Twist", &HumanIK::spineTwist, 1.0);
    register_property<HumanIK, float>("Spine Twist Offset", &HumanIK::spineTwistOffset, 0);

    register_property<HumanIK, float>("Max Stair Height", &HumanIK::rayCastHeight, 0.5);

    register_property<HumanIK, bool>("Enable Head Tracker", &HumanIK::headTrackerEnabled, false);
    register_property<HumanIK, bool>("Enable Left Hand Tracker", &HumanIK::leftHandTrackerEnabled, false);
    register_property<HumanIK, bool>("Enable Right Hand Tracker", &HumanIK::rightHandTrackerEnabled, false);
    register_property<HumanIK, bool>("Enable Hip Tracker", &HumanIK::hipTrackerEnabled, true);
    register_property<HumanIK, bool>("Enable Left Foot Tracker", &HumanIK::leftFootTrackerEnabled, true);
    register_property<HumanIK, bool>("Enable Right Foot Tracker", &HumanIK::rightFootTrackerEnabled, true);

    register_property<HumanIK, bool>("Manual Update", &HumanIK::manualUpdate, false);

    register_method("_physics_process", &HumanIK::_physics_process);
    register_method("_process", &HumanIK::_process);
    register_method("_ready", &HumanIK::_ready);

    register_method("performBodyIK", &HumanIK::performBodyIK);
    register_method("performLeftHandIK", &HumanIK::performLeftHandIK);
    register_method("performRightHandIK", &HumanIK::performRightHandIK);
    register_method("performLeftFootIK", &HumanIK::performLeftFootIK);
    register_method("performRightFootIK", &HumanIK::performRightFootIK);

    register_method("setHeadTargetNode", &HumanIK::setHeadTargetNode);
    register_method("setHandLeftTargetNode", &HumanIK::setHandLeftTargetNode);
    register_method("setHandRightTargetNode", &HumanIK::setHandRightTargetNode);
    register_method("setHipTargetNode", &HumanIK::setHipTargetNode);
    register_method("setFootLeftTargetNode", &HumanIK::setFootLeftTargetNode);
    register_method("setFootRightTargetNode", &HumanIK::setFootRightTargetNode);

    register_method("setFalling", &HumanIK::setFalling);
    register_method("setManualUpdate", &HumanIK::setManualUpdate);
    register_method("update", &HumanIK::update);
}

HumanIK::HumanIK()
{
}

HumanIK::~HumanIK()
{
    // add your cleanup here
}

void HumanIK::_init()
{
    // initialize any variables here
    time = 0.0;
    validBody = false;
    validLeftHand = false;
    validRightHand = false;
    validLeftFoot = false;
    validRightFoot = false;

    headTrackerEnabled = false;
    leftHandTrackerEnabled = false;
    rightHandTrackerEnabled = false;
    hipTrackerEnabled = true;
    leftFootTrackerEnabled = true;
    rightFootTrackerEnabled = true;

    skeleton = nullptr;

    headTargetSpatial = nullptr;
    handLeftTargetSpatial = nullptr;
    handRightTargetSpatial = nullptr;
    hipTargetSpatial = nullptr;
    footLeftTargetSpatial = nullptr;
    footRightTargetSpatial = nullptr;

    Node *parent = this->get_parent();
    skeletonPath = NodePath("..");
    if (parent != nullptr)
    {
        skeletonPath = parent->get_path();
    }
    skeleton = nullptr;

    Transform zeroTransform = Transform();
    headTarget = zeroTransform;
    handLeftTarget = zeroTransform;
    handRightTarget = zeroTransform;
    hipTarget = zeroTransform;
    footLeftTarget = zeroTransform;
    footRightTarget = zeroTransform;

    NodePath zeroPath = NodePath("");
    headTargetNode = zeroPath;
    handLeftTargetNode = zeroPath;
    handRightTargetNode = zeroPath;
    hipTargetNode = zeroPath;
    footLeftTargetNode = zeroPath;
    footRightTargetNode = zeroPath;

    headTargetSpatial = nullptr;
    handLeftTargetSpatial = nullptr;
    handRightTargetSpatial = nullptr;
    hipTargetSpatial = nullptr;
    footLeftTargetSpatial = nullptr;
    footRightTargetSpatial = nullptr;

    floorHeight = 0.0;

    headBone = -1;
    neckBones = PoolIntArray(); //array of neck bones
    shoulderLeftBone = -1;
    upperArmLeftBone = -1;
    forearmLeftBone = -1;
    handLeftBone = -1;
    shoulderRightBone = -1;
    upperArmRightBone = -1;
    forearmRightBone = -1;
    handRightBone = -1;
    spineBones = PoolIntArray(); //array of spine bones
    hipBone = -1;
    thighLeftBone = -1;
    shinLeftBone = -1;
    footLeftBone = -1;
    thighRightBone = -1;
    shinRightBone = -1;
    footRightBone = -1;

    crouchBendFactor = 50.0;
    hunchFactor = 0.0;
    shoulderFlex = 10.0;
    armStretch = 0.1;
    legStretch = 0.1;

    elbowAngleOffset = -90;
    elbowRestOffset = -45.0;
    shoulderTwist = 50;
    wristTwist = 66.666;
    elbowPoleOffset = Vector3(0, 0, -60);
    elbowPositionBias = Vector3(-100, 0, -50.0); //debug values
    elbowRotationBias = 50.0;
    kneeAngleOffset = 180.0;
    kneeRestOffset = 0.0;
    hipTwist = 0;
    ankleTwist = 20;
    kneePoleOffset = Vector3(-30, -20, 0);
    kneePositionBias = Vector3(0, 0, 30);
    kneeRotationBias = 10.0;

    shoulderTurnSensitivity = 50.0;
    shoulderTurnLimit = 0.0;
    hipTurnSpeed = 5.0;
    hipTurnLimit = 0.0;
    turnResetSpeed = 1.0;
    spineTwist = 1.0;
    spineTwistOffset = 0;

    String boneAssignmentsGroup = "";
    String IKAdjustmentsGroup = "";

    spineCurve = std::vector<Transform>(7);
    curveDist = 0;
    spineLength = 0;
    curveAxis = Vector2(-1, 0);

    leftStepping = true;
    rightStepping = true;
    falling = false;
    leftProgress = 0; //How far along the leg is in taking the step. Measured in time. Goes from 0 to leftStepTime.
    rightProgress = 0;
    stepTime = 0.5;
    stepHeight = 0.5;
    leftFootLength = 0.25;
    rightFootLength = 0.25;
    footScoreThreshold = 0.1;
    centerScoreThreshold = 0.01;
    footOrientationThreshold = 1;
    relaxAmount = 30;
    leftStepType = 0; //enumerated value where 0 is planted, 1 is ideal step, 2 is centered step, and 3 is perigee step
    rightStepType = 0;

    rayCastHeight = 0.5;
    manualUpdate = false;
}

void HumanIK::_ready()
{
    //Optimize for MMD defaults
    setSkeletonPath(getSkeletonPath());
    if (skeleton != nullptr)
    {
        if (hipBone == -1)
        {
            hipBone = skeleton->find_bone("Hips");
            // if (hipBone >= 0)
            // {
            //     skeleton->set_bone_disable_rest(hipBone, true);
            // }
        }
        if (spineBones.size() == 0)
        {
            int64_t spine = skeleton->find_bone("Spine");
            int64_t chest = skeleton->find_bone("Chest");
            if (spine != -1)
            {
                spineBones.append(spine);
            }
            if (chest != -1)
            {
                spineBones.append(chest);
            }
        }
        if (neckBones.size() == 0)
        {
            int64_t neck = skeleton->find_bone("Neck");
            if (neck != -1)
            {
                neckBones.append(neck);
            }
        }
        if (headBone == -1)
        {
            headBone = skeleton->find_bone("Head");
        }
        if (shoulderRightBone == -1)
        {
            shoulderRightBone = skeleton->find_bone("Right shoulder");
        }
        if (upperArmRightBone == -1)
        {
            upperArmRightBone = skeleton->find_bone("Right arm");
        }
        if (forearmRightBone == -1)
        {
            forearmRightBone = skeleton->find_bone("Right elbow");
        }
        if (handRightBone == -1)
        {
            handRightBone = skeleton->find_bone("Right wrist");
        }
        if (thighRightBone == -1)
        {
            thighRightBone = skeleton->find_bone("Right leg");
        }
        if (shinRightBone == -1)
        {
            shinRightBone = skeleton->find_bone("Right knee");
        }
        if (footRightBone == -1)
        {
            footRightBone = skeleton->find_bone("Right ankle");
        }
        if (shoulderLeftBone == -1)
        {
            shoulderLeftBone = skeleton->find_bone("Left shoulder");
        }
        if (upperArmLeftBone == -1)
        {
            upperArmLeftBone = skeleton->find_bone("Left arm");
        }
        if (forearmLeftBone == -1)
        {
            forearmLeftBone = skeleton->find_bone("Left elbow");
        }
        if (handLeftBone == -1)
        {
            handLeftBone = skeleton->find_bone("Left wrist");
        }
        if (thighLeftBone == -1)
        {
            thighLeftBone = skeleton->find_bone("Left leg");
        }
        if (shinLeftBone == -1)
        {
            shinLeftBone = skeleton->find_bone("Left knee");
        }
        if (footLeftBone == -1)
        {
            footLeftBone = skeleton->find_bone("Left ankle");
        }
    }

    //Setters and getters have validation checks that need to be run;
    //setSkeletonPath(getSkeletonPath());

    setHeadTargetNode(getHeadTargetNode());
    setHandLeftTargetNode(getHandLeftTargetNode());
    setHandRightTargetNode(getHandRightTargetNode());
    setHipTargetNode(getHipTargetNode());
    setFootLeftTargetNode(getFootLeftTargetNode());
    setFootRightTargetNode(getFootRightTargetNode());

    setHeadBone(getHeadBone());
    setNeckBones(getNeckBones());
    setShoulderLeftBone(getShoulderLeftBone());
    setUpperArmLeftBone(getUpperArmLeftBone());
    setForearmLeftBone(getForearmLeftBone());
    setHandLeftBone(getHandLeftBone());
    setShoulderRightBone(getShoulderRightBone());
    setUpperArmRightBone(getUpperArmRightBone());
    setForearmRightBone(getForearmRightBone());
    setHandRightBone(getHandRightBone());
    setSpineBones(getSpineBones());
    setHipBone(getHipBone());
    setThighLeftBone(getThighLeftBone());
    setShinLeftBone(getShinLeftBone());
    setFootLeftBone(getFootLeftBone());
    setThighRightBone(getThighRightBone());
    setShinRightBone(getShinRightBone());
    setFootRightBone(getFootRightBone());
}

void HumanIK::_physics_process(float delta)
{
    time += delta;
    // hipTrace();
    // legTrace(delta);
    // performBodyIK();
    // performLeftHandIK();
    // performRightHandIK();
    // performLeftFootIK();
    // performRightFootIK();
}

void HumanIK::_process(float delta)
{
    hipTrace();
    legTrace(delta);
    if (!manualUpdate)
    {
        update();
    }
}

void HumanIK::update()
{
    if (headTrackerEnabled)
    {
        performBodyIK();
        // if (skeleton != nullptr)
        // {
        //     // printf(skeleton->get_name().alloc_c_string());
        //     // printf("\n");
        // }
        performLeftHandIK();
        performRightHandIK();
        performLeftFootIK();
        performRightFootIK();

        oldHeadTarget = headTarget;
        oldHipTarget = hipTarget;
        oldHandLeftTarget = handLeftTarget;
        oldHandRightTarget = handRightTarget;
        oldFootLeftTarget = footLeftTarget;
        oldFootRightTarget = footRightTarget;
    }
}

bool HumanIK::validateBone(int64_t bone)
{
    bool output = false;
    if (skeleton != nullptr)
    {
        if (-1 < bone && bone < skeleton->get_bone_count())
        {
            output = true;
        }
    }
    return output;
}

bool HumanIK::validateBody()
{
    bool output = false;
    if (validateBone(headBone) && validateBone(hipBone))
    {
        Transform headRestTransform = skeleton->get_bone_rest(headBone);
        int64_t prevBone = headBone;

        Array sortedNeckBones = Array(neckBones);
        sortedNeckBones.sort();
        for (int64_t bone = 0; bone < neckBones.size(); bone++)
        {
            int64_t parentBone = skeleton->get_bone_parent(prevBone);
            if (validateBone(parentBone) && parentBone == int64_t(sortedNeckBones.pop_back()))
            {
                prevBone = parentBone;
                headRestTransform = skeleton->get_bone_rest(parentBone) * headRestTransform;
            }
            else
            {
                return false;
            }
        }

        Array sortedSpineBones = Array(spineBones);
        sortedSpineBones.sort();
        for (int64_t bone = 0; bone < spineBones.size(); bone++)
        {
            int64_t parentBone = skeleton->get_bone_parent(prevBone);
            if (validateBone(parentBone) && parentBone == int64_t(sortedSpineBones.pop_back()))
            {
                prevBone = parentBone;
                headRestTransform = skeleton->get_bone_rest(parentBone) * headRestTransform;
            }
            else
            {
                return false;
            }
        }

        int64_t parentBone = skeleton->get_bone_parent(prevBone);
        if (parentBone == hipBone && spineBones.size() > 0)
        {
            output = true;

            tPoseHipsDist = headRestTransform.get_origin().length();
            tPoseHipVector = headRestTransform.get_origin();
            tPoseHeadLocal = headRestTransform;
            headRestTransform = skeleton->get_bone_rest(parentBone) * headRestTransform;
            tPoseHeight = headRestTransform.get_origin()[1];
        }
        tPoseHipTransformLocalToHead = headRestTransform.affine_inverse() * skeleton->get_bone_rest(hipBone);
        tPoseHead = headRestTransform;
    }

    if (output == true)
    {
        calcSpineCurve();
    }

    return output;
}

bool HumanIK::validateLeftHand()
{
    bool output = false;
    if (validateBone(handLeftBone) && validateBone(shoulderLeftBone))
    {
        Array sortedSpineBones = Array(spineBones);
        sortedSpineBones.sort();
        Variant topSpine = sortedSpineBones.pop_back();

        int64_t shoulderParent = skeleton->get_bone_parent(shoulderLeftBone);
        int64_t upperArmParent = skeleton->get_bone_parent(upperArmLeftBone);
        int64_t forearmParent = skeleton->get_bone_parent(forearmLeftBone);
        int64_t handParent = skeleton->get_bone_parent(handLeftBone);

        if (handParent == forearmLeftBone && forearmParent == upperArmLeftBone && upperArmParent == shoulderLeftBone && Variant(shoulderParent) == topSpine)
        {
            output = true;
        }
    }
    return output;
}

bool HumanIK::validateRightHand()
{
    bool output = false;
    if (validateBone(handRightBone) && validateBone(shoulderRightBone))
    {
        Array sortedSpineBones = Array(spineBones);
        sortedSpineBones.sort();
        Variant topSpine = sortedSpineBones.pop_back();

        int64_t shoulderParent = skeleton->get_bone_parent(shoulderRightBone);
        int64_t upperArmParent = skeleton->get_bone_parent(upperArmRightBone);
        int64_t forearmParent = skeleton->get_bone_parent(forearmRightBone);
        int64_t handParent = skeleton->get_bone_parent(handRightBone);
        if (handParent == forearmRightBone && forearmParent == upperArmRightBone && upperArmParent == shoulderRightBone && Variant(shoulderParent) == topSpine)
        {
            output = true;
        }
    }
    return output;
}

bool HumanIK::validateLeftFoot()
{
    bool output = false;
    if (validateBone(hipBone) && validateBone(footLeftBone))
    {
        int64_t thighLeftParent = skeleton->get_bone_parent(thighLeftBone);
        int64_t shinLeftParent = skeleton->get_bone_parent(shinLeftBone);
        int64_t footLeftParent = skeleton->get_bone_parent(footLeftBone);

        if (footLeftParent == shinLeftBone && shinLeftParent == thighLeftBone && thighLeftParent == hipBone)
        {
            output = true;
            footLeftTarget = skeleton->get_global_transform() * skeleton->get_bone_global_pose(footLeftBone);
            leftPlant = footLeftTarget;
        }

        int64_t toe = skeleton->find_bone("Left toe");
        if (toe > 0 && skeleton->get_bone_parent(toe) == footLeftBone)
        {
            leftFootLength = skeleton->get_bone_rest(toe).get_origin().length();
        }
    }
    return output;
}

bool HumanIK::validateRightFoot()
{
    bool output = false;
    if (validateBone(hipBone) && validateBone(footRightBone))
    {
        int64_t thighRightParent = skeleton->get_bone_parent(thighRightBone);
        int64_t shinRightParent = skeleton->get_bone_parent(shinRightBone);
        int64_t footRightParent = skeleton->get_bone_parent(footRightBone);

        if (footRightParent == shinRightBone && shinRightParent == thighRightBone && thighRightParent == hipBone)
        {
            output = true;
            footRightTarget = skeleton->get_global_transform() * skeleton->get_bone_global_pose(footRightBone);
            rightPlant = footRightTarget;
        }

        int64_t toe = skeleton->find_bone("Right toe");
        if (toe > 0 && skeleton->get_bone_parent(toe) == footRightBone)
        {
            rightFootLength = skeleton->get_bone_rest(toe).get_origin().length();
        }
    }
    return output;
}

void HumanIK::performBodyIK(float influence)
{
    if (validBody && headTrackerEnabled && (headTarget != oldHeadTarget || hipTarget != oldHipTarget))
    {

        Transform headPose = skeleton->get_bone_global_pose(headBone);
        Transform headRest = skeleton->get_bone_rest(headBone);
        Transform hipPose = skeleton->get_bone_global_pose(hipBone);
        Transform hipRest = skeleton->get_bone_rest(hipBone);

        std::vector<Transform> chainBones = std::vector<Transform>(spineBones.size() + neckBones.size());
        for (int i = 0; i < spineBones.size(); i++)
        {
            Transform oldTransform = skeleton->get_bone_rest(spineBones[i]);
            chainBones[i] = oldTransform;
        }
        for (int i = 0; i < neckBones.size(); i++)
        {
            Transform oldTransform = skeleton->get_bone_rest(neckBones[i]);
            chainBones[i + spineBones.size()] = oldTransform;
        }
        headPose = headPose.interpolate_with(hipRest.affine_inverse() * skeleton->get_global_transform().affine_inverse() * headTarget, influence);
        hipPose = hipPose.interpolate_with(hipRest.affine_inverse() * skeleton->get_global_transform().affine_inverse() * hipTarget, influence);
        Transform torsoT = hipPose;
        std::vector<Transform> solution = solveISFABRIK(hipPose, chainBones, headRest, headPose, spineCurve, curveDist, spineLength, 0.0001, 20, spineTwist, spineTwistOffset);
        for (int i = 0; i < spineBones.size(); i++)
        {
            Transform t = solution[i];
            skeleton->set_bone_custom_pose(spineBones[i], t);
            torsoT = torsoT * chainBones[i] * t;
        }
        for (int i = 0; i < neckBones.size(); i++)
        {
            Transform t = solution[i + spineBones.size()];
            skeleton->set_bone_custom_pose(neckBones[i], t);
            torsoT = torsoT * chainBones[i + spineBones.size()] * t;
        }
        skeleton->set_bone_custom_pose(headBone, Transform(((torsoT * headRest).affine_inverse() * headPose).get_basis(), Vector3()));
        skeleton->set_bone_custom_pose(hipBone, hipPose); //for some reason I can't set the global pose
    }
}

void HumanIK::performLeftHandIK(float influence)
{
    if (validLeftHand && leftHandTrackerEnabled && (handLeftTarget != oldHandLeftTarget || headTarget != oldHeadTarget || hipTarget != oldHipTarget))
    {
        if (handLeftTargetSpatial != nullptr)
        {
            handLeftTarget = handLeftTargetSpatial->get_global_transform();
        }
        Transform handLeftRest = skeleton->get_bone_rest(handLeftBone);
        Transform shoulderGlobal = skeleton->get_bone_global_pose(shoulderLeftBone);
        Transform upperArmLeftPose = skeleton->get_bone_rest(upperArmLeftBone);
        Transform forearmLeftPose = skeleton->get_bone_rest(forearmLeftBone);
        Transform handLeftPose = skeleton->get_bone_global_pose(handLeftBone).interpolate_with(skeleton->get_global_transform().affine_inverse() * handLeftTarget, influence);
        //a lot of things are multiplied by -1 or have their z multiplied by -1 to reflect everything so that the right hand and the left hand are properly mirrored
        TrigSolution solution = solveTrigIK(shoulderGlobal, upperArmLeftPose, forearmLeftPose, handLeftRest, handLeftPose, elbowAngleOffset, elbowRestOffset, shoulderTwist, wristTwist, elbowPoleOffset, Vector3(elbowPositionBias[0], elbowPositionBias[1], elbowPositionBias[2]), elbowRotationBias);
        skeleton->set_bone_custom_pose(upperArmLeftBone, solution.limb1);
        skeleton->set_bone_custom_pose(forearmLeftBone, solution.limb2);

        Transform armT = shoulderGlobal * upperArmLeftPose * solution.limb1 * forearmLeftPose * solution.limb2 * handLeftRest;
        handLeftPose = armT.affine_inverse() * handLeftPose;
        if (handLeftPose.get_origin().length() > armStretch)
        {
            float unstretch = 1 - smoothCurve(handLeftPose.get_origin().length() - armStretch, 0.025);
            handLeftPose.set_origin(handLeftPose.get_origin().normalized() * armStretch);
        }
        skeleton->set_bone_custom_pose(handLeftBone, handLeftPose);
    }
}

void HumanIK::performRightHandIK(float influence)
{
    if (validRightHand && rightHandTrackerEnabled && (handRightTarget != oldHandRightTarget || headTarget != oldHeadTarget || hipTarget != oldHipTarget))
    {
        if (handRightTargetSpatial != nullptr)
        {
            handRightTarget = handRightTargetSpatial->get_global_transform();
        }
        Transform handRightRest = skeleton->get_bone_rest(handRightBone);
        Transform shoulderGlobal = skeleton->get_bone_global_pose(shoulderRightBone);
        Transform upperArmRightPose = skeleton->get_bone_rest(upperArmRightBone);
        Transform forearmRightPose = skeleton->get_bone_rest(forearmRightBone);
        Transform handRightPose = skeleton->get_bone_global_pose(handRightBone).interpolate_with(skeleton->get_global_transform().affine_inverse() * handRightTarget, influence);
        //a lot of things are multiplied by -1 or have their z multiplied by -1 to reflect everything so that the right hand and the left hand are properly mirrored
        TrigSolution solution = solveTrigIK(shoulderGlobal, upperArmRightPose, forearmRightPose, handRightRest, handRightPose, elbowAngleOffset * -1, elbowRestOffset * -1, shoulderTwist, wristTwist, Vector3(elbowPoleOffset[0], elbowPoleOffset[1] * -1, elbowPoleOffset[2] * -1), Vector3(1 * elbowPositionBias[0], elbowPositionBias[1] * -1, elbowPositionBias[2] * -1), elbowRotationBias);
        skeleton->set_bone_custom_pose(upperArmRightBone, solution.limb1);
        skeleton->set_bone_custom_pose(forearmRightBone, solution.limb2);

        Transform armT = shoulderGlobal * upperArmRightPose * solution.limb1 * forearmRightPose * solution.limb2 * handRightRest;
        handRightPose = armT.affine_inverse() * handRightPose;
        if (handRightPose.get_origin().length() > armStretch)
        {
            float unstretch = 1 - smoothCurve(handRightPose.get_origin().length() - armStretch, 0.025);
            handRightPose.set_origin(handRightPose.get_origin().normalized() * armStretch);
        }
        skeleton->set_bone_custom_pose(handRightBone, handRightPose);
    }
}

void HumanIK::performLeftFootIK(float influence)
{
    if (validLeftFoot && leftFootTrackerEnabled && (footLeftTarget != oldFootLeftTarget || hipTarget != oldHipTarget))
    {
        Transform footLeftRest = skeleton->get_bone_rest(footLeftBone);
        Transform hipGlobal = skeleton->get_bone_global_pose(hipBone);
        Transform thighLeftPose = skeleton->get_bone_rest(thighLeftBone);
        Transform shinLeftPose = skeleton->get_bone_rest(shinLeftBone);
        Transform footLeftPose = skeleton->get_bone_global_pose(footLeftBone).interpolate_with(skeleton->get_global_transform().affine_inverse() * footLeftTarget, influence);
        //a lot of things are multiplied by -1 or have their z multiplied by -1 to reflect everything so that the right foot and the left foot are properly mirrored
        TrigSolution solution = solveTrigIK(hipGlobal, thighLeftPose, shinLeftPose, footLeftRest, footLeftPose, kneeAngleOffset, kneeRestOffset, hipTwist, ankleTwist, kneePoleOffset, kneePositionBias, kneeRotationBias);
        skeleton->set_bone_custom_pose(thighLeftBone, solution.limb1);
        skeleton->set_bone_custom_pose(shinLeftBone, solution.limb2);

        Transform legT = hipGlobal * thighLeftPose * solution.limb1 * shinLeftPose * solution.limb2 * footLeftRest;
        footLeftPose = legT.affine_inverse() * footLeftPose;
        if (footLeftPose.get_origin().length() > legStretch)
        {
            float unstretch = 1 - smoothCurve(footLeftPose.get_origin().length() - legStretch, 0.025);
            footLeftPose.set_origin((footLeftPose.get_origin().normalized() * legStretch) * unstretch);
        }
        skeleton->set_bone_custom_pose(footLeftBone, footLeftPose);
    }
}

void HumanIK::performRightFootIK(float influence)
{
    if (validRightFoot && rightFootTrackerEnabled && (footRightTarget != oldFootRightTarget || hipTarget != oldHipTarget))
    {
        Transform footRightRest = skeleton->get_bone_rest(footRightBone);
        Transform hipGlobal = skeleton->get_bone_global_pose(hipBone);
        Transform thighRightPose = skeleton->get_bone_rest(thighRightBone);
        Transform shinRightPose = skeleton->get_bone_rest(shinRightBone);
        Transform footRightPose = skeleton->get_bone_global_pose(footRightBone).interpolate_with(skeleton->get_global_transform().affine_inverse() * footRightTarget, influence);
        //a lot of things are multiplied by -1 or have their z multiplied by -1 to reflect everything so that the right foot and the left foot are properly mirrored
        TrigSolution solution = solveTrigIK(hipGlobal, thighRightPose, shinRightPose, footRightRest, footRightPose, kneeAngleOffset * -1, kneeRestOffset * -1, hipTwist, ankleTwist, Vector3(kneePoleOffset[0], kneePoleOffset[1] * -1, kneePoleOffset[2] * -1), Vector3(kneePositionBias[0], kneePositionBias[1] * -1, kneePositionBias[2] * -1), kneeRotationBias * -1);
        skeleton->set_bone_custom_pose(thighRightBone, solution.limb1);
        skeleton->set_bone_custom_pose(shinRightBone, solution.limb2);

        Transform legT = hipGlobal * thighRightPose * solution.limb1 * shinRightPose * solution.limb2 * footRightRest;
        footRightPose = legT.affine_inverse() * footRightPose;
        if (footRightPose.get_origin().length() > legStretch)
        {
            float unstretch = 1 - smoothCurve(footRightPose.get_origin().length() - legStretch, 0.025);
            footRightPose.set_origin((footRightPose.get_origin().normalized() * legStretch) * unstretch);
        }
        skeleton->set_bone_custom_pose(footRightBone, footRightPose);
    }
}

NodePath HumanIK::getSkeletonPath()
{
    return skeletonPath;
}

void HumanIK::setSkeletonPath(NodePath newValue)
{
    skeletonPath = newValue;
    Node *skeletonNode = get_node(skeletonPath);
    skeleton = Object::cast_to<Skeleton>(skeletonNode);

    // if (skeleton != nullptr && hipBone >= 0)
    // {
    //     skeleton->set_bone_disable_rest(hipBone, true);
    // }
}

NodePath HumanIK::getHeadTargetNode()
{
    return headTargetNode;
}
NodePath HumanIK::getHandLeftTargetNode()
{
    return handLeftTargetNode;
}
NodePath HumanIK::getHandRightTargetNode()
{
    return handRightTargetNode;
}
NodePath HumanIK::getHipTargetNode()
{
    return hipTargetNode;
}
NodePath HumanIK::getFootLeftTargetNode()
{
    return footLeftTargetNode;
}
NodePath HumanIK::getFootRightTargetNode()
{
    return footRightTargetNode;
}

void HumanIK::setHeadTargetNode(NodePath newValue)
{
    headTargetNode = newValue;
    Node *targetNode = get_node_or_null(newValue);
    headTargetSpatial = Object::cast_to<Spatial>(targetNode);
    headTrackerEnabled = (headTargetSpatial != nullptr);
}
void HumanIK::setHandLeftTargetNode(NodePath newValue)
{
    handLeftTargetNode = newValue;
    Node *targetNode = get_node_or_null(newValue);
    handLeftTargetSpatial = Object::cast_to<Spatial>(targetNode);
    leftHandTrackerEnabled = (handLeftTargetSpatial != nullptr);
}
void HumanIK::setHandRightTargetNode(NodePath newValue)
{
    handRightTargetNode = newValue;
    Node *targetNode = get_node_or_null(newValue);
    handRightTargetSpatial = Object::cast_to<Spatial>(targetNode);
    rightHandTrackerEnabled = (handRightTargetSpatial != nullptr);
}
void HumanIK::setHipTargetNode(NodePath newValue)
{
    hipTargetNode = newValue;
    Node *targetNode = get_node_or_null(newValue);
    hipTargetSpatial = Object::cast_to<Spatial>(targetNode);
}
void HumanIK::setFootLeftTargetNode(NodePath newValue)
{
    footLeftTargetNode = newValue;
    Node *targetNode = get_node_or_null(newValue);
    footLeftTargetSpatial = Object::cast_to<Spatial>(targetNode);
}
void HumanIK::setFootRightTargetNode(NodePath newValue)
{
    footRightTargetNode = newValue;
    Node *targetNode = get_node_or_null(newValue);
    footRightTargetSpatial = Object::cast_to<Spatial>(targetNode);
}

int64_t HumanIK::getHeadBone()
{
    return headBone;
}
PoolIntArray HumanIK::getNeckBones()
{
    return neckBones;
}
int64_t HumanIK::getShoulderLeftBone()
{
    return shoulderLeftBone;
}
int64_t HumanIK::getUpperArmLeftBone()
{
    return upperArmLeftBone;
}
int64_t HumanIK::getForearmLeftBone()
{
    return forearmLeftBone;
}
int64_t HumanIK::getHandLeftBone()
{
    return handLeftBone;
}
int64_t HumanIK::getShoulderRightBone()
{
    return shoulderRightBone;
}
int64_t HumanIK::getUpperArmRightBone()
{
    return upperArmRightBone;
}
int64_t HumanIK::getForearmRightBone()
{
    return forearmRightBone;
}
int64_t HumanIK::getHandRightBone()
{
    return handRightBone;
}
PoolIntArray HumanIK::getSpineBones()
{
    return spineBones;
}
int64_t HumanIK::getHipBone()
{
    return hipBone;
}
int64_t HumanIK::getThighLeftBone()
{
    return thighLeftBone;
}
int64_t HumanIK::getShinLeftBone()
{
    return shinLeftBone;
}
int64_t HumanIK::getFootLeftBone()
{
    return footLeftBone;
}
int64_t HumanIK::getThighRightBone()
{
    return thighRightBone;
}
int64_t HumanIK::getShinRightBone()
{
    return shinRightBone;
}
int64_t HumanIK::getFootRightBone()
{
    return footRightBone;
}

void HumanIK::setHeadBone(int64_t newValue)
{
    headBone = newValue;
    validBody = validateBody();
}
void HumanIK::setNeckBones(PoolIntArray newValue)
{
    neckBones = newValue;
    validBody = validateBody();
}
void HumanIK::setShoulderLeftBone(int64_t newValue)
{
    shoulderLeftBone = newValue;
    validLeftHand = validateLeftHand();
}
void HumanIK::setUpperArmLeftBone(int64_t newValue)
{
    upperArmLeftBone = newValue;
    validLeftHand = validateLeftHand();
}
void HumanIK::setForearmLeftBone(int64_t newValue)
{
    forearmLeftBone = newValue;
    validLeftHand = validateLeftHand();
}
void HumanIK::setHandLeftBone(int64_t newValue)
{
    handLeftBone = newValue;
    validLeftHand = validateLeftHand();
}
void HumanIK::setShoulderRightBone(int64_t newValue)
{
    shoulderRightBone = newValue;
    validRightHand = validateRightHand();
}
void HumanIK::setUpperArmRightBone(int64_t newValue)
{
    upperArmRightBone = newValue;
    validRightHand = validateRightHand();
}
void HumanIK::setForearmRightBone(int64_t newValue)
{
    forearmRightBone = newValue;
    validRightHand = validateRightHand();
}
void HumanIK::setHandRightBone(int64_t newValue)
{
    handRightBone = newValue;
    validRightHand = validateRightHand();
}
void HumanIK::setSpineBones(PoolIntArray newValue)
{
    spineBones = newValue;
    validBody = validateBody();
    validLeftHand = validateLeftHand();
    validRightHand = validateRightHand();
}
void HumanIK::setHipBone(int64_t newValue)
{
    hipBone = newValue;
    validBody = validateBody();
    validLeftFoot = validateLeftFoot();
    validRightFoot = validateRightFoot();
}
void HumanIK::setThighLeftBone(int64_t newValue)
{
    thighLeftBone = newValue;
    validLeftFoot = validateLeftFoot();
}
void HumanIK::setShinLeftBone(int64_t newValue)
{
    shinLeftBone = newValue;
    validLeftFoot = validateLeftFoot();
}
void HumanIK::setFootLeftBone(int64_t newValue)
{
    footLeftBone = newValue;
    validLeftFoot = validateLeftFoot();
}
void HumanIK::setThighRightBone(int64_t newValue)
{
    thighRightBone = newValue;
    validRightFoot = validateRightFoot();
}
void HumanIK::setShinRightBone(int64_t newValue)
{
    shinRightBone = newValue;
    validRightFoot = validateRightFoot();
}
void HumanIK::setFootRightBone(int64_t newValue)
{
    footRightBone = newValue;
    validRightFoot = validateRightFoot();
}

// void HumanIK::_validate_property(PropertyInfo &property) const
// {

//     if (property.name == "root_bone" || property.name == "tip_bone")
//     {

//         if (skeleton)
//         {

//             String names("--,");
//             for (int i = 0; i < skeleton->get_bone_count(); i++)
//             {
//                 if (i > 0)
//                     names += ",";
//                 names += skeleton->get_bone_name(i);
//             }

//             property.hint = PROPERTY_HINT_ENUM;
//             property.hint_string = names;
//         }
//         else
//         {

//             property.hint = PROPERTY_HINT_NONE;
//             property.hint_string = "";
//         }
//     }
// }

HumanIK::TrigSolution HumanIK::solveTrigIK(Transform parent, Transform limb1, Transform limb2, Transform leaf, Transform target, float angleOffset, float restOffset, float l1Twist, float l2Twist, Vector3 poleOffset, Vector3 targetPositionInfluence, float targetRotationInfluence)
{
    //l1 is the final custom transform of the bone nearest the root
    //l2 is the final custom transform of the bone nearest the leaf
    Transform l1 = Transform();
    Transform l2 = Transform();

    Transform localTarget = ((parent * limb1).affine_inverse() * target);
    Vector3 targetVector = localTarget.get_origin(); //targetVector pointing from the root to the leaf

    //Find out which direction the limbs are pointing in their resting position
    Vector3 l1Vector = limb2.get_origin();
    Vector3 l2Vector = leaf.get_origin();

    //Everything we need to use trignometry to find the angles in the triangle made by the bent limb.
    float limb1Length = l1Vector.length();
    float limb2Length = l2Vector.length();
    float totalDistance = targetVector.length();

    /* -- Elbow/Knee Angles and Poles --
    We want to solve for the direction elbow or knee direction deterministically, meaning every possible leaf position in a limb's range of motion has one and only one angle that we'll consider to be the most natural looking.
    If we imagine the limb's total range of motion as an incomplete sphere, we can represent the most natural direction for the elbow or knee to be pointing as unit vectors in the sphere.
    According to the Hair Ball Theorem, it is impossible draw vectors on a sphere such that there are no poles.
    In testing, a pole naturally occurs opposite the resting position of a limb and the limb acts erratically when the limb approaches this area.
    In the following block of code, we offset the the limbs before solving IK such that the poles lie outside the limb's natural range of motion.
    */
    Basis poleRotation = Basis(poleOffset * (Math_PI / 180));
    l1 = l1 * poleRotation;
    Vector3 rotatedL1Vector = poleRotation.xform(l1Vector);

    //Straighten the limb and point it at the target position
    Vector3 cross1;
    float dot1;
    crossDot(cross1, dot1, rotatedL1Vector, targetVector);
    l1.rotate_basis(cross1.normalized(), safeACOS(dot1));

    Vector3 cross2;
    float dot2;
    crossDot(cross2, dot2, l2Vector, l1Vector);
    l2.rotate_basis(cross2.normalized(), safeACOS(dot2));

    //Figure out where the elbow or knee is supposed to bend. The axis1 is the same as axis2, but one is local to l1 and the other is local to l2
    Vector3 axis2 = l2.get_basis()[0].rotated(l2.get_basis()[1], Math_PI * angleOffset / 180);
    if (abs(l2Vector[2]) > abs(l2Vector[1]))
    {
        axis2 = l2.get_basis()[0].rotated(l2.get_basis()[2], Math_PI * angleOffset / 180);
    }
    Vector3 axis1 = l1.xform(axis2);

    if (totalDistance <= abs(limb1Length - limb2Length))
    {
        //Solving for the angles are impossible here, so just get as close as possible
        l2.rotate_basis(axis2, safeACOS(dot2) + Math_PI);
    }
    else if (totalDistance < limb1Length + limb2Length)
    {
        //Law of Cosines
        float a1 = safeACOS((powf(limb1Length, 2) + powf(totalDistance, 2) - powf(limb2Length, 2)) / (2 * limb1Length * totalDistance));
        float a2 = safeACOS((powf(limb1Length, 2) + powf(limb2Length, 2) - powf(totalDistance, 2)) / (2 * limb1Length * limb2Length)) - Math_PI;
        l1.rotate_basis(axis1, a1);
        l2.rotate_basis(axis2, a2);
    }

    //The resulting elbow/knee angle isn't always the most natural, so we tweak it below based on the target's position and rotation
    Vector3 positionDiff = (parent.affine_inverse() * target).get_origin() - ((limb1 * limb2) * leaf).get_origin(); //local to parent

    float totalPositionOffset = (smoothCurve(positionDiff[0]) * targetPositionInfluence[0] * Math_PI / 180) + (smoothCurve(positionDiff[1]) * targetPositionInfluence[1] * Math_PI / 180) + (smoothCurve(positionDiff[2]) * targetPositionInfluence[2] * Math_PI / 180);
    l1.rotate_basis(targetVector.normalized(), (restOffset * Math_PI / 180) + totalPositionOffset);

    // Dude rotations are hard. WTF
    float totalRotationOffset = 0;
    Transform restLeaf = l1 * limb2 * l2 * leaf; //Transform of the leaf given ik of l1 and l2, but not taking into account the rotations of the target
    Vector3 xLeaf = restLeaf.xform(l2Vector.normalized());
    Vector3 xTarget = localTarget.get_basis().xform(l2Vector.normalized());
    //at rest, the leaf has the same angle as l2, and in this case l2 is bent to x2
    Vector3 restRejection = vectorRejection(xLeaf, targetVector);
    Vector3 leafRejection = vectorRejection(xTarget, targetVector);
    float rejectionRotation = safeACOS(restRejection.normalized().dot(leafRejection.normalized()));

    //measure twistingint twistIndex = 0;
    int twistIndex = 0;
    if (abs(l2Vector[1]) > abs(l2Vector[0]) && abs(l2Vector[1]) > abs(l2Vector[2]))
    {

        twistIndex = 1;
    }
    else if (abs(l2Vector[2]) > abs(l2Vector[1]) && abs(l2Vector[2]) > abs(l2Vector[0]))
    {
        twistIndex = 2;
    }
    float twistRotation = localTarget.get_basis().orthonormalized().get_euler()[twistIndex] * -1;
    if (restRejection.normalized().cross(leafRejection.normalized()).dot(targetVector.normalized()) > 0)
    {
        totalRotationOffset = 0 + rejectionRotation;
    }
    else
    {
        totalRotationOffset = 0 - rejectionRotation;
    }

    totalRotationOffset *= targetRotationInfluence / 100;
    l1.rotate_basis(targetVector.normalized(), totalRotationOffset);

    //Detwist - Account for lack of twist joints and put some of the twisting on the elbow/knee joint instead of keeping it all the wrists and shoulder

    Vector3 newL2Vector = l2.xform(l2Vector).normalized();
    float l2TwistAmount = localTarget.get_basis().orthonormalized().get_euler()[twistIndex] * (l2Twist / 100);
    l2.rotate_basis(newL2Vector, l2TwistAmount);

    Vector3 newL1Vector = l1.xform(l1Vector).normalized();
    float l1TwistAmount = (l1 * limb2 * l2).get_basis().orthonormalized().get_euler()[twistIndex] * (l1Twist / 100);
    l1.rotate_basis(newL1Vector, l1TwistAmount);
    l2.rotate_basis(l1Vector.normalized(), l1TwistAmount * -1);

    return TrigSolution{l1, l2};
}

float HumanIK::smoothCurve(float number, float modifier)
{
    return number / (abs(number) + modifier);
}

float HumanIK::sinusoidalInterpolation(float number)
{ //makes a linear interpolation from 0 to 1 interpolate sinusoidally
    return cos(Math_PI * number) * -0.5 + 1;
}

Vector3 HumanIK::vectorRejection(Vector3 v, Vector3 normal)
{
    float normalLength = normal.length();
    Vector3 proj = (normal.dot(v) / normalLength) * (normal / normalLength);
    return v - proj;
}

std::vector<Transform> HumanIK::solveISFABRIK(Transform parent, std::vector<Transform> restTransforms, Transform leaf, Transform target, std::vector<Transform> curveTransforms, float curveDist, float maxDist, float threshold, int loopLimit, float twist, float twistOffset) //Start Interpolated FABRIK. Interpolate chain towards idealized curve, then solve FABRIK to fill in the rest. Helps to prevent locking and make sure joints bend in the right direction without constraints.
{
    std::vector<Transform> transformArray = std::vector<Transform>(restTransforms.size(), Transform());
    if (restTransforms.size() > 0 && curveTransforms.size() == restTransforms.size() && curveDist != maxDist)
    {
        float x = parent.xform(Transform(restTransforms[0]).get_origin()).distance_to(target.get_origin());
        float y = (maxDist - x) / (maxDist - curveDist);
        if (y > 0)
        {
            y = sqrtf(y);
        }

        //FABRIK only works on points, so we turn all our bone transforms into points.
        std::vector<BonePoint> bonePoints = std::vector<BonePoint>(restTransforms.size());
        float maxLength = 0;

        Transform globalT = parent;
        Transform interpolatedT = parent;
        Vector3 rootPoint = Vector3();
        Vector3 leafPoint = Vector3();

        for (int i = 0; i < restTransforms.size(); i++)
        {
            Transform t = Transform(restTransforms[i]);
            Transform c = Transform(curveTransforms[i]);
            Transform interp = t.interpolate_with(c, y);

            globalT = globalT * t;
            interpolatedT = interpolatedT * interp;
            Vector3 globalOrigin = globalT.get_origin();
            globalOrigin = interpolatedT.get_origin();

            float length = 0;
            if (i == restTransforms.size() - 1)
            {
                //On the last item, the leaf is the child
                length = leaf.get_origin().length();
                leafPoint = globalT.xform(leaf.get_origin());
            }
            else
            {
                length = Transform(restTransforms[i + 1]).get_origin().length();
            }
            maxLength += length;

            bonePoints[i] = BonePoint(length, t, globalOrigin);

            if (i == 0)
            {
                //The parent doesn't rotate, so the root of the point chain is actually the root of the first child
                rootPoint = globalOrigin;
            }
        }

        Transform localT = parent;
        Transform globalLeaf = globalT * leaf;
        Quat totalRotation = (globalLeaf.affine_inverse() * target).get_basis(); //Total rotational difference between the target and the leaf at rest expressed in Quaternions

        float targetLength = (target.get_origin() - rootPoint).length();
        if (targetLength >= maxDist)
        {

            //Now we have to take all the points in bonePoints and turn them back into Transforms.
            //Because each bone will be pointing directly at the bone in front of it on the chain this is pretty simple.
            //However, there's still one degree of freedom that needs to be taken care of and that is the rotation of the bone along the axis in which it's pointing a.k.a. twist.
            //We calculate twist by using Quaternions to figure out the smoothest transition from the root twist amount to the leaf twist amount
            float totalTwistLength = parent.get_origin().distance_to(globalLeaf.get_origin()); //total length of the limb including the parent. If we didn't include the parent then the first joint wouldn't twist and we want twisting to be as spread out as possible.
            float localTwistLength = 0;                                                        //how far up the limb we are. used to figure out how much we should be twisting

            //variables reused by every bone in the iteration
            Vector3 direction = Vector3();
            Vector3 restDirection = Vector3();
            Vector3 cross = Vector3();
            float dot = 1;

            for (int i = 0; i < bonePoints.size(); i++)
            {
                BonePoint b = bonePoints[i];
                //First we rotate everything to match the target
                localTwistLength += b.length;
                Transform t;
                t.basis = Quat().slerp(totalRotation, (localTwistLength * twist + twistOffset) / totalTwistLength); //The output transform
                //Then we rotate everything back so that -Z is still -Z. The twist should stay.
                crossDot(cross, dot, t.get_basis()[2], Vector3(0, 0, 1));
                t.rotate_basis(cross, safeACOS(dot) * -1); //don't know why the -1 fixed everything but it works now lol

                localT = localT * b.transform;
                if (i == bonePoints.size() - 1)
                {
                    //the last
                    direction = localT.xform_inv(target.get_origin());
                    restDirection = leaf.get_origin();
                }
                else
                {
                    direction = localT.xform_inv(bonePoints[i + 1].point);
                    restDirection = bonePoints[i + 1].transform.get_origin();
                }
                crossDot(cross, dot, restDirection, direction);
                t.rotate_basis(cross, safeACOS(dot));
                transformArray[i] = t.orthonormalized();
                localT = localT * t;
            }

            //Just point the whole chain at the target
            Vector3 leafRestDirection = (parent * transformArray[0]).xform_inv(leafPoint);
            //restDirection = Vector3(0, 0, -1);
            Vector3 targetDirection = (parent * transformArray[0]).xform_inv(target.get_origin());
            Transform localTransform = transformArray[0];
            crossDot(cross, dot, leafRestDirection, targetDirection);
            localTransform.rotate_basis(cross.normalized(), safeACOS(dot));
            transformArray[0] = localTransform;
        }
        else
        {
            solveFABRIKPoints(bonePoints, rootPoint, target.get_origin(), threshold, loopLimit); //does FABRIK on it and bonePoints through pass-by-reference

            //Now we have to take all the points in bonePoints and turn them back into Transforms.
            //Because each bone will be pointing directly at the bone in front of it on the chain this is pretty simple.
            //However, there's still one degree of freedom that needs to be taken care of and that is the rotation of the bone along the axis in which it's pointing a.k.a. twist.
            //We calculate twist by using Quaternions to figure out the smoothest transition from the root twist amount to the leaf twist amount
            float totalTwistLength = parent.get_origin().distance_to(globalLeaf.get_origin()); //total length of the limb including the parent. If we didn't include the parent then the first joint wouldn't twist and we want twisting to be as spread out as possible.
            float localTwistLength = 0;                                                        //how far up the limb we are. used to figure out how much we should be twisting

            //variables reused by every bone in the iteration
            Vector3 direction = Vector3();
            Vector3 restDirection = Vector3();
            Vector3 cross = Vector3();
            float dot = 1;

            for (int i = 0; i < bonePoints.size(); i++)
            {
                BonePoint b = bonePoints[i];
                //First we rotate everything to match the target
                localTwistLength += b.length;
                Transform t;
                t.basis = Quat().slerp(totalRotation, (localTwistLength * twist + twistOffset) / totalTwistLength); //The output transform
                //Then we rotate everything back so that -Z is still -Z. The twist should stay.
                crossDot(cross, dot, t.get_basis()[2], Vector3(0, 0, 1));
                t.rotate_basis(cross, safeACOS(dot) * -1); //don't know why the -1 fixed everything but it works now lol

                localT = localT * b.transform;
                if (i == bonePoints.size() - 1)
                {
                    //the last
                    direction = localT.xform_inv(target.get_origin());
                    restDirection = leaf.get_origin();
                }
                else
                {
                    direction = localT.xform_inv(bonePoints[i + 1].point);
                    restDirection = bonePoints[i + 1].transform.get_origin();
                }
                crossDot(cross, dot, restDirection, direction);
                t.rotate_basis(cross, safeACOS(dot));
                transformArray[i] = t.orthonormalized();
                localT = localT * t;
            }
        }
    }
    return transformArray;
}

void HumanIK::calcSpineCurve()
{
    if (validBody)
    {
        //get a copy of all the bones
        Transform head = skeleton->get_bone_rest(headBone);

        Transform totalTransform = Transform();
        std::vector<BonePoint> bonePoints = std::vector<BonePoint>(spineBones.size() + neckBones.size());
        float totalLength = head.get_origin().length();
        for (int i = 0; i < spineBones.size(); i++)
        {
            Transform t = skeleton->get_bone_rest(spineBones[i]);
            if (i > 0)
            { // exclude the hip bone transform
                totalTransform = totalTransform * t;
                totalLength += t.get_origin().length();
            }
            else
            {
                totalTransform = t.get_basis(); //ignore translation
            }

            if (i < spineBones.size() - 1)
            {
                float l = skeleton->get_bone_rest(spineBones[i + 1]).get_origin().length();
                bonePoints[i] = BonePoint(l, t, totalTransform.get_origin());
            }
            else if (neckBones.size() > 0)
            {
                float l = skeleton->get_bone_rest(neckBones[0]).get_origin().length();
                bonePoints[i] = BonePoint(l, t, totalTransform.get_origin());
            }
            else
            {
                float l = head.get_origin().length();
                bonePoints[i] = BonePoint(l, t, totalTransform.get_origin());
            }
        }
        for (int i = 0; i < neckBones.size(); i++)
        {
            Transform t = skeleton->get_bone_rest(neckBones[i]);

            totalTransform = totalTransform * t;
            totalLength += t.get_origin().length();

            if (i < neckBones.size() - 1)
            {
                float l = skeleton->get_bone_rest(neckBones[i + 1]).get_origin().length();
                bonePoints[i + spineBones.size()] = BonePoint(l, t, totalTransform.get_origin());
            }
            else
            {
                float l = head.get_origin().length();
                bonePoints[i + spineBones.size()] = BonePoint(l, t, totalTransform.get_origin());
            }
        }

        spineLength = totalTransform.xform(head.get_origin()).length(); //at rest how far is the base of the head from the root of the chain (the hip bone)
        double radius = totalLength / Math_PI;
        float totalAngle = 0;
        float totalBAngle = 0;
        float lastB = 0;
        Vector3 nCurveAxis = Vector3(curveAxis.normalized()[0], curveAxis.normalized()[1], 0);
        spineCurve.assign(bonePoints.size(), Transform());

        for (int i = bonePoints.size() - 1; i >= 0; i--)
        {
            float a = 2 * safeASIN(bonePoints[i].length / (2 * radius));
            float b = (Math_PI - a) / 2;
            totalAngle += a;

            if (i < bonePoints.size() - 1)
            {
                Transform bone = bonePoints[i + 1].transform;
                Vector3 idealAngle = Vector3(0, 0, -1).rotated(nCurveAxis, lastB + b); //TODO: change -1 to 1
                Vector3 cross = Vector3();
                float dot = 0;
                crossDot(cross, dot, bone.get_basis()[2], idealAngle);
                bone.rotate_basis(cross, safeACOS(dot));
                spineCurve[i + 1] = bone.orthonormalized();
            }

            if (i == 0)
            {
                Transform bone = bonePoints[i].transform;
                float xa = 2 * Math_PI - totalAngle;
                curveDist = 2 * radius * sin(2 * xa);
                float xb = (Math_PI - xa) / 2;
                Vector3 idealAngle = Vector3(0, 0, 1).rotated(nCurveAxis, (xb + b));
                Vector3 cross = Vector3();
                float dot = 0;
                crossDot(cross, dot, bone.get_basis()[2], idealAngle);
                bone.rotate_basis(cross, safeACOS(dot));
                spineCurve[i] = bone.orthonormalized();
            }

            lastB = b;
        }
    }
}

Vector3 HumanIK::fitPointToLine(Vector3 point, Vector3 goal, float length)
{
    Vector3 output = Vector3();
    Vector3 direction = point - goal;
    output = direction.normalized() * length;
    output = goal + output;
    return output;
}

void HumanIK::solveFABRIKPoints(std::vector<HumanIK::BonePoint> &bonePoints, Vector3 rootPoint, Vector3 goal, float threshold, int loopLimit)
{
    int loop = 0;
    while (abs((goal - bonePoints.back().point).length() - bonePoints.back().length) > threshold && loop < loopLimit)
    {
        loop++;
        // Backward
        Vector3 target = goal;                          //The very first bone in the backwards iteration needs to reach the goal
        for (int i = bonePoints.size() - 1; i > 0; i--) //don't place the root bone point
        {
            BonePoint b = bonePoints[i];
            b.point = fitPointToLine(b.point, target, b.length);
            bonePoints[i] = b;
            target = b.point; //Where this new point ended up will be the target for the next bone in the chain
        }

        // Forward
        target = rootPoint;                         //The root bone must touch the root point
        for (int i = 1; i < bonePoints.size(); i++) //i starts at 1 because the root bone is fixed in place
        {
            BonePoint b = bonePoints[i];
            b.point = fitPointToLine(b.point, target, b.transform.get_origin().length());
            bonePoints[i] = b;
            target = b.point;
        }
    }
}

std::vector<Transform> HumanIK::solveFABRIK(Transform parent, std::vector<Transform> restTransforms, Transform leaf, Transform target, float threshold, int loopLimit, float twist, float twistOffset)
{
    std::vector<Transform> transformArray = restTransforms;
    std::vector<BonePoint> bonePoints = std::vector<BonePoint>(restTransforms.size());

    //FABRIK only works on points, so we turn all our bone transforms into points.
    if (restTransforms.size() > 0)
    {
        float maxLength = 0;

        Transform globalT = parent;
        Vector3 rootPoint = Vector3();

        for (int i = 0; i < restTransforms.size(); i++)
        {
            Transform t = restTransforms[i];
            globalT = globalT * t;
            Vector3 globalOrigin = globalT.get_origin();

            float length = 0;
            if (i == restTransforms.size() - 1)
            {
                //On the last item, the leaf is the child
                length = leaf.get_origin().length();
            }
            else
            {
                length = Transform(restTransforms[i + 1]).get_origin().length();
            }
            maxLength += length;

            bonePoints[i] = BonePoint(length, t, globalOrigin);

            if (i == 0)
            {
                //The parent doesn't rotate, so the root of the point chain is actually the root of the first child
                rootPoint = globalOrigin;
            }
        }

        float targetLength = (target.get_origin() - rootPoint).length();
        if (targetLength >= maxLength)
        {
            //Just make it straight
            Transform localTransform = Transform();
            for (int i = 0; i < transformArray.size(); i++)
            {
                Transform t = Transform();
                Vector3 targetDirection = Vector3(0, 0, -1);
                Vector3 direction = Vector3(0, 0, -1);
                if (i == 0)
                {
                    targetDirection = Transform(restTransforms[i]).xform_inv(parent.xform_inv(target.get_origin()));
                }
                else
                {
                    // targetDirection = Transform(restTransforms[i]).get_origin();
                }
                if (i == transformArray.size() - 1)
                {
                    direction = leaf.get_origin();
                }
                else
                {
                    direction = Transform(restTransforms[i + 1]).get_origin();
                }
                targetDirection.normalize();
                direction.normalize();
                Vector3 cross;
                float dot;
                crossDot(cross, dot, direction, targetDirection);
                t.rotate_basis(cross.normalized(), safeACOS(dot));
                transformArray[i] = t;
            }
        }
        else
        {
            solveFABRIKPoints(bonePoints, rootPoint, target.get_origin(), threshold, loopLimit); //does FABRIK on it and bonePoints through pass-by-reference

            //Now we have to take all the points in bonePoints and turn them back into Transforms.
            //Because each bone will be pointing directly at the bone in front of it on the chain this is pretty simple.
            //However, there's still one degree of freedom that needs to be taken care of and that is the rotation of the bone along the axis in which it's pointing a.k.a. twist.
            //We calculate twist by using Quaternions to figure out the smoothest transition from the root twist amount to the leaf twist amount
            Transform localT = parent;
            Transform globalLeaf = globalT * leaf;
            Quat totalRotation = (globalLeaf.affine_inverse() * target).get_basis();           //Total rotational difference between the target and the leaf at rest expressed in Quaternions
            float totalTwistLength = parent.get_origin().distance_to(globalLeaf.get_origin()); //total length of the limb including the parent. If we didn't include the parent then the first joint wouldn't twist and we want twisting to be as spread out as possible.
            float localTwistLength = 0;                                                        //how far up the limb we are. used to figure out how much we should be twisting

            //variables reused by every bone in the iteration
            Vector3 direction = Vector3();
            Vector3 restDirection = Vector3();
            Vector3 cross = Vector3();
            float dot = 1;

            for (int i = 0; i < bonePoints.size(); i++)
            {
                BonePoint b = bonePoints[i];

                //First we rotate everything to match the target
                localTwistLength += b.length;
                Transform t = Transform(Quat().slerp(totalRotation, (localTwistLength * twist + twistOffset) / totalTwistLength)); //The output transform
                //Then we rotate everything back so that -Z is still -Z. The twist should stay.
                crossDot(cross, dot, t.get_basis()[2], Vector3(0, 0, 1));
                t.rotate(cross.normalized(), safeACOS(dot) * -1); //don't know why the -1 fixed everything but god damn it works now

                localT = localT * Transform(b.transform);
                if (i == bonePoints.size() - 1)
                {
                    //the last
                    direction = localT.xform_inv(target.get_origin());
                    restDirection = leaf.get_origin();
                }
                else
                {
                    direction = localT.xform_inv(bonePoints[i + 1].point);
                    restDirection = bonePoints[i + 1].transform.get_origin();
                }
                crossDot(cross, dot, restDirection, direction);
                t.rotate_basis(cross.normalized(), safeACOS(dot));
                transformArray[i] = t.orthonormalized();
                localT = localT * t;
            }
        }
    }
    return transformArray;
}

void HumanIK::crossDot(Vector3 &cross, float &dot, Vector3 srcVector, Vector3 targetVector)
{
    srcVector.normalize();
    targetVector.normalize();
    cross = srcVector.cross(targetVector);
    dot = srcVector.dot(targetVector);
    if (cross.length() == 0)
    {
        if (dot < 0)
        { //the vectors are 180 degrees away from each other
            cross = srcVector.cross(Vector3(0, 1, 0));
            if (targetVector[0] == 0)
            {
                cross = srcVector.cross(Vector3(1, 0, 0));
            }
        }
        else
        {
            cross = Vector3(0, 0, 1);
            dot = 1;
        }
    }
    cross.normalize();
}

float HumanIK::safeACOS(float f)
{
    return acos(std::max(-1.0f, std::min(1.0f, f)));
}
float HumanIK::safeASIN(float f)
{
    return asin(std::max(-1.0f, std::min(1.0f, f)));
}

void HumanIK::floorTrace()
{
    //HOW THE FUCK DO I RAYCAST
}

void HumanIK::hipTrace()
{
    if (headTrackerEnabled && headTargetSpatial != nullptr && hipTrackerEnabled)
    {
        headTarget = headTargetSpatial->get_global_transform();
        if (hipTargetSpatial != nullptr)
        {
            hipTarget = hipTargetSpatial->get_global_transform();
        }
        else
        {
            //hipTarget = skeleton->get_global_transform() * skeleton->get_bone_global_pose(hipBone);
            if (tPoseHeight != 0)
            {
                float playspaceHeightOffset = std::max(0.0f, tPoseHeight - skeleton->get_bone_global_pose(headBone).get_origin()[1] + floorHeight);
                float heightOffsetRatio = std::max(0.0f, std::min(1.0f, playspaceHeightOffset / tPoseHeight));

                float crouchAmount = 0;
                if (crouchBendFactor == 100)
                {
                    crouchAmount = 1;
                }
                else
                {
                    crouchAmount = abs(heightOffsetRatio / ((100 - crouchBendFactor) / 100));
                }

                Basis targetBasis = headTarget.get_basis();
                Vector3 up = tPoseHead.get_basis().xform_inv(Vector3(0, 1, 0));
                Vector3 forward = tPoseHead.get_basis().xform_inv(Vector3(0, 0, -1));
                Vector3 influence = targetBasis.xform(forward * -1).linear_interpolate(targetBasis.xform(up), heightOffsetRatio).normalized();
                Vector3 rotateVector = (influence.cross(Vector3(0, -1, 0))).normalized();

                Vector3 cross;
                float dot;
                Transform uprightHead = headTarget;
                crossDot(cross, dot, uprightHead.get_basis().xform(up), Vector3(0, 1, 0));
                uprightHead.rotate_basis(cross, safeACOS(dot));

                if (rotateVector.length() > 0 && crouchAmount > 0)
                {
                    Transform straightHip = uprightHead * tPoseHipTransformLocalToHead; //Spine perfectly straight
                    float rotateAngle = safeASIN(std::min(0.999f, crouchAmount));
                    Transform idealHip = straightHip;
                    idealHip.set_origin(idealHip.get_origin() - uprightHead.get_origin()); //make the head the point around which the rotation on the next line takes place
                    idealHip.rotate(rotateVector, rotateAngle);
                    idealHip.set_origin(idealHip.get_origin() + uprightHead.get_origin());
                    Transform idealHunch = Transform(idealHip.rotated(rotateVector, Math_PI / -2).get_basis(), uprightHead.get_origin()); //Hips are perpendicular to the head and are pulled all the way up to the head
                    Transform calculatedHips = idealHip.interpolate_with(idealHunch, (hunchFactor / 100) * heightOffsetRatio);
                    hipTarget.set_origin(calculatedHips.get_origin());
                    hipTarget = hipTarget.interpolate_with(calculatedHips, hipTurnSpeed / 100);
                    crossDot(cross, dot, hipTarget.xform_inv(uprightHead.get_origin()), calculatedHips.xform_inv(uprightHead.get_origin()));
                    hipTarget.set_basis(hipTarget.get_basis() * Basis().rotated(cross, safeACOS(dot) * -1));
                }
                else
                {
                    Transform straightHip = uprightHead * tPoseHipTransformLocalToHead;
                    hipTarget = hipTarget.interpolate_with(straightHip, hipTurnSpeed / 100);
                    hipTarget.set_origin(straightHip.get_origin());
                }
            }
        }
    }
}

void HumanIK::legTrace(float delta)
{
    //Each Foot and the center of balance gets an "unsteadiness" score
    //There's a line called the balance line connecting both feet. The center of balance's score is determined by how close it is to that line where a 0 means its on the line.
    //Each foot's score is determined by how close it is to the center of balance where a 0 means its at the center of balance.
    //Whenever the center of balance's score goes above a certain threshold, the foot with the highest score takes a step.
    //Whenever the left foot crosses onto the center of balance's right side, it takes a step. Same goes for the right foot and the left side.
    //Whenever a foot's placement is no longer within reach of the foot, it takes a step.
    //Whenever the difference between a foot's orientation and the center of gravity's orientation exceeds a certain threshold, the foot takes a step.
    //When one foot is taking a step, the other foot goes into "planted" mode where it's range is slightly extended by allowing the ankle to move, it won't take a step for any reason other than its foot placement is out of range.
    //When taking a step, the foot leaves the ground for about half a second and aims to land about shoulderwidth from where the center of gravity is such that the center of gravity is over the balance line. The foot interpolates the landing spot towards its resting position if it looks like it may be out of range.
    if (leftFootTrackerEnabled && footLeftTargetSpatial != nullptr)
    {
        footLeftTarget = footLeftTargetSpatial->get_global_transform();
    }
    if (rightFootTrackerEnabled && footRightTargetSpatial != nullptr)
    {
        footRightTarget = footRightTargetSpatial->get_global_transform();
    }

    if (validBody && validLeftFoot && validRightFoot && footLeftTargetSpatial == nullptr && footRightTargetSpatial == nullptr && leftFootTrackerEnabled && rightFootTrackerEnabled)
    {
        float playspaceHeightOffset = std::max(0.0f, tPoseHeight - skeleton->get_bone_global_pose(headBone).get_origin()[1] + floorHeight);
        float heightOffsetRatio = std::max(0.0f, std::min(1.0f, playspaceHeightOffset / tPoseHeight));

        //update the center of gravity
        Transform localHead = skeleton->get_bone_global_pose(headBone); // local to skeleton
        Transform globalHead = skeleton->get_global_transform() * localHead;
        Transform localHips = skeleton->get_bone_global_pose(hipBone); // local to skeleton
        //Transform globalHips = skeleton->get_global_transform() * localHips;
        Transform restHips = skeleton->get_bone_rest(hipBone);
        Transform globalRestHips = (skeleton->get_global_transform() * restHips);

        Transform leftFootRest = skeleton->get_bone_rest(footLeftBone);
        Transform rightFootRest = skeleton->get_bone_rest(footRightBone);
        Transform localLeftFootRest = skeleton->get_bone_rest(thighLeftBone) * skeleton->get_bone_rest(shinLeftBone) * leftFootRest;
        Transform localRightFootRest = skeleton->get_bone_rest(thighRightBone) * skeleton->get_bone_rest(shinRightBone) * rightFootRest;

        float restFloor = (restHips * localLeftFootRest).get_origin()[1] + floorHeight - skeleton->get_global_transform().get_origin()[1]; //how high the ankle joint is at rest
        Transform leftPerigee = localLeftFootRest;                                                                                         //the closest point on the floor to the start of the leg IK chain
        Transform rightPerigee = localRightFootRest;

        Transform hipVertical = restHips;
        Vector3 cross = skeleton->get_global_transform().get_basis()[1];
        float dot = 0;
        Vector3 forwardVector = restHips.get_basis().xform_inv(Vector3(0, 0, 1));
        Vector3 pelvisForward = vectorRejection(hipTarget.get_basis().xform(forwardVector), cross).normalized();
        Vector3 pelvisForwardRest = vectorRejection(globalRestHips.get_basis().xform(forwardVector), cross).normalized();
        crossDot(cross, dot, pelvisForwardRest, pelvisForward);
        hipVertical.rotate(cross, safeACOS(dot));

        leftPerigee.set_basis(skeleton->get_global_transform().get_basis() * hipVertical.get_basis() * leftPerigee.get_basis()); //make transforms global
        rightPerigee.set_basis(skeleton->get_global_transform().get_basis() * hipVertical.get_basis() * rightPerigee.get_basis());
        leftPerigee.orthonormalize();
        rightPerigee.orthonormalize();
        leftPerigee.set_origin(Vector3((hipTarget * skeleton->get_bone_rest(thighLeftBone)).get_origin()[0], restFloor, (hipTarget * skeleton->get_bone_rest(thighLeftBone)).get_origin()[2]));
        rightPerigee.set_origin(Vector3((hipTarget * skeleton->get_bone_rest(thighRightBone)).get_origin()[0], restFloor, (hipTarget * skeleton->get_bone_rest(thighRightBone)).get_origin()[2]));

        Vector3 centerPos = headTarget.get_origin().linear_interpolate(hipTarget.get_origin(), 0.75); //Origin placed about where the belly button is
        Vector3 oldCenterFloorPos = centerFloor.get_origin();
        Vector3 centerFloorPos = Vector3(centerPos[0], restFloor, centerPos[2]);
        centerFloor = Transform(globalHead.get_basis(), Vector3(centerPos[0], restFloor, centerPos[2])); //Orientation taken from head.
        Vector3 centerVelocity = centerFloorPos - oldCenterFloorPos;
        Vector3 nextCenterPos = centerFloorPos + centerVelocity;
        Vector3 hipFloorPos = Vector3(hipTarget.get_origin()[0], restFloor, hipTarget.get_origin()[2]);

        float leftLegLength = skeleton->get_bone_rest(shinLeftBone).get_origin().length() + leftFootRest.get_origin().length();
        float rightLegLength = skeleton->get_bone_rest(shinRightBone).get_origin().length() + rightFootRest.get_origin().length();

        if (falling)
        { //Dangle the feet
            dangleLeftFoot(leftPerigee);
            dangleRightFoot(rightPerigee);
        }
        else
        {
            Vector3 leftFootPos = Vector3(footLeftTarget.get_origin()[0], restFloor, footLeftTarget.get_origin()[2]);
            Vector3 rightFootPos = Vector3(footRightTarget.get_origin()[0], restFloor, footRightTarget.get_origin()[2]);

            //The ideal steps places the center on the balance line. The centerscore will be a perfect 0 if the step lands, and the foot that just moved will be near the center of gravity
            //The centered steps places the feet in their resting positions around the center of gravity
            //The perigee steps are just the closest spot on the ground to the legs. This is used if literally everything else is out of reach.
            float idealLeftDistance = leftPerigee.get_origin().distance_to(hipFloorPos);
            float idealRightDistance = rightPerigee.get_origin().distance_to(hipFloorPos);
            Vector3 leftCenterVector = leftPlant.get_origin() + (leftPerigee.get_origin() - centerFloorPos);
            Vector3 rightCenterVector = rightPlant.get_origin() + (rightPerigee.get_origin() - centerFloorPos);
            Vector3 idealLeftPos = fitPointToLine(rightCenterVector, centerFloorPos, (idealLeftDistance * -1));
            Vector3 idealRightPos = fitPointToLine(leftCenterVector, centerFloorPos, (idealRightDistance * -1));
            Vector3 balancedLeftPos = fitPointToLine(rightCenterVector, centerFloorPos, (centerFloorPos - rightFootPos).length() * -1);
            Vector3 balancedRightPos = fitPointToLine(leftCenterVector, centerFloorPos, (centerFloorPos - leftFootPos).length() * -1);
            idealLeftPos = idealLeftPos.linear_interpolate(balancedLeftPos, smoothCurve(centerVelocity.length(), 0.2));
            idealRightPos = idealRightPos.linear_interpolate(balancedRightPos, smoothCurve(centerVelocity.length(), 0.2));
            //idealLeftPos = balancedLeftPos;
            //idealRightPos = balancedRightPos;
            // printf("idealLeftPos: %f %f %f\n", idealLeftPos[0], idealLeftPos[1], idealLeftPos[2]);
            // printf("idealRightPos: %f %f %f\n", idealRightPos[0], idealRightPos[1], idealRightPos[2]);
            Transform idealLeftStep = Transform(leftPerigee.get_basis(), idealLeftPos);
            Transform idealRightStep = Transform(rightPerigee.get_basis(), idealRightPos);

            Vector3 centeredOffset = centerFloorPos - hipFloorPos;
            // printf("centeredOffset: %f %f %f\n", centeredOffset[0], centeredOffset[1], centeredOffset[2]);
            // printf("centerFloorPos: %f %f %f\n", centerFloorPos[0], centerFloorPos[1], centerFloorPos[2]);
            // printf("hipFloorPos: %f %f %f\n", hipFloorPos[0], hipFloorPos[1], hipFloorPos[2]);

            Transform centeredLeftStep = leftPerigee;
            Transform centeredRightStep = rightPerigee;
            centeredLeftStep.set_origin(centeredLeftStep.get_origin() + centeredOffset);
            centeredRightStep.set_origin(centeredRightStep.get_origin() + centeredOffset);

            Vector3 leftThighOrigin = skeleton->get_bone_global_pose(thighLeftBone).get_origin();
            Vector3 rightThighOrigin = skeleton->get_bone_global_pose(thighRightBone).get_origin();
            Vector3 globalLeftThighOrigin = skeleton->get_global_transform().xform(leftThighOrigin);
            Vector3 globalRightThighOrigin = skeleton->get_global_transform().xform(rightThighOrigin);

            float leftScore = nextCenterPos.distance_squared_to(leftFootPos) - powf(idealLeftDistance, 2);
            float rightScore = nextCenterPos.distance_squared_to(rightFootPos) - powf(idealRightDistance, 2);
            float centerScore = vectorRejection(centerFloorPos - leftFootPos, rightFootPos - leftFootPos).length_squared();

            Vector3 relativeLeft = leftFootPos - centerPos;
            Vector3 relativeRight = rightFootPos - centerPos;
            float leftAngle = pelvisForward.cross(relativeLeft)[1]; //all these vectors are on the floor plane, so their cross product should be a vector that either points up or down
            float rightAngle = pelvisForward.cross(relativeRight)[1];
            Vector3 leftLegRange = globalLeftThighOrigin - footLeftTarget.get_origin();
            Vector3 rightLegRange = globalRightThighOrigin - footRightTarget.get_origin();
            Transform leftFootGlobal = skeleton->get_bone_global_pose(footLeftBone);
            Transform rightFootGlobal = skeleton->get_bone_global_pose(footRightBone);
            // printf(" - - center threshold: %f leg threshold: %f\n", centerScoreThreshold, footScoreThreshold);
            // printf(" - - center score: %f left score: %f right score: %f\n", centerScore, leftScore, rightScore);
            //leftStepping = rightStepping = true; //DEBUG
            if (!leftStepping && !rightStepping)
            {
                leftStepType = rightStepType = 0;
                //check if any of the scores goes beyond the threshold and react accordingly
                if (leftAngle < 0 || leftLegRange.length() > leftLegLength || safeACOS(leftPerigee.get_basis()[2].dot(footLeftTarget.get_basis()[2])) > footOrientationThreshold) //checks if foot is on wrong side, is unable to reach the target, or is twisted too much
                {
                    leftStepping = true;
                }
                else if (rightAngle > 0 || rightLegRange.length() > rightLegLength || safeACOS(rightPerigee.get_basis()[2].dot(footRightTarget.get_basis()[2])) > footOrientationThreshold)
                {
                    rightStepping = true;
                }
                else if (centerScore > centerScoreThreshold)
                {
                    if (leftScore > rightScore)
                    {
                        leftStepping = true;
                    }
                    else
                    {
                        rightStepping = true;
                    }
                }
                else if (leftScore > footScoreThreshold)
                {
                    leftStepping = true;
                }
                else if (rightScore > footScoreThreshold)
                {
                    rightStepping = true;
                }
            }
            else if (leftStepping && !rightStepping)
            {
                //calc Left Step and plant right foot
                float idealAngle = pelvisForward.cross(idealLeftPos - centerPos)[1];
                if (globalLeftThighOrigin.distance_to(idealLeftPos) < leftLegLength && idealAngle >= 0)
                {
                    if (leftStepType != 1)
                    { //means it was already in the process of stepping towards a different target
                        leftStepType = 1;
                    }
                    progressLeftStep(idealLeftStep, delta, centerVelocity.length(), heightOffsetRatio);
                }
                else if (globalLeftThighOrigin.distance_to(centeredLeftStep.get_origin()) < leftLegLength)
                {
                    if (leftStepType != 2)
                    { //means it was already in the process of stepping towards a different target
                        leftStepType = 2;
                    }
                    progressLeftStep(centeredLeftStep, delta, centerVelocity.length(), heightOffsetRatio);
                }
                else if (globalLeftThighOrigin.distance_to(leftPerigee.get_origin()) < leftLegLength)
                {
                    if (leftStepType != 3)
                    { //means it was already in the process of stepping towards a different target
                        leftStepType = 3;
                    }
                    progressLeftStep(leftPerigee, delta, centerVelocity.length(), heightOffsetRatio);
                }
                else
                {
                    if (leftStepType != 4)
                    { //means it was already in the process of stepping towards a different target
                        leftStepType = 4;
                    }
                    dangleLeftFoot(leftPerigee);
                }

                if (rightLegRange.length() > rightLegLength + legStretch)
                {
                    rightStepping = true;
                }
            }
            else if (!leftStepping && rightStepping)
            {
                //calc Right Step and plant left foot
                float idealAngle = pelvisForward.cross(idealRightPos - centerPos)[1];
                if (globalRightThighOrigin.distance_to(idealRightPos) < rightLegLength && idealAngle <= 0)
                {
                    if (rightStepType != 1)
                    { //means it was already in the process of stepping towards a different target
                        rightStepType = 1;
                    }
                    progressRightStep(idealRightStep, delta, centerVelocity.length(), heightOffsetRatio);
                }
                else if (globalRightThighOrigin.distance_to(centeredRightStep.get_origin()) < rightLegLength)
                {
                    if (rightStepType != 2)
                    { //means it was already in the process of stepping towards a different target
                        rightStepType = 2;
                    }
                    progressRightStep(centeredRightStep, delta, centerVelocity.length(), heightOffsetRatio);
                }
                else if (globalRightThighOrigin.distance_to(rightPerigee.get_origin()) < rightLegLength)
                {
                    if (rightStepType != 3)
                    { //means it was already in the process of stepping towards a different target
                        rightStepType = 3;
                    }
                    progressRightStep(rightPerigee, delta, centerVelocity.length(), heightOffsetRatio);
                }
                else
                {
                    if (rightStepType != 4)
                    { //means it was already in the process of stepping towards a different target
                        rightStepType = 4;
                    }
                    dangleRightFoot(rightPerigee);
                }

                if (leftLegRange.length() > leftLegLength + legStretch) //checks if foot is on wrong side, is unable to reach the target, or is twisted too much
                {
                    leftStepping = true;
                }
            }
            else
            {
                //calc both steps, but try to place both feet at their rest positions
                if (globalLeftThighOrigin.distance_to(centeredLeftStep.get_origin()) <= leftLegLength && globalLeftThighOrigin.distance_to(footLeftTarget.get_origin()) <= leftLegLength)
                {
                    if (leftStepType != 2)
                    { //means it was already in the process of stepping towards a different target
                        leftStepType = 2;
                    }
                    progressLeftStep(centeredLeftStep, delta, centerVelocity.length(), heightOffsetRatio);
                }
                else if (globalLeftThighOrigin.distance_to(leftPerigee.get_origin()) <= leftLegLength && globalLeftThighOrigin.distance_to(footLeftTarget.get_origin()) <= leftLegLength)
                {
                    if (leftStepType != 3)
                    { //means it was already in the process of stepping towards a different target
                        leftStepType = 3;
                    }
                    progressLeftStep(leftPerigee, delta, centerVelocity.length(), heightOffsetRatio);
                }
                else
                {
                    if (leftStepType != 4)
                    { //means it was already in the process of stepping towards a different target
                        leftStepType = 4;
                    }
                    dangleLeftFoot(leftPerigee);
                }

                if (globalRightThighOrigin.distance_to(centeredRightStep.get_origin()) <= rightLegLength && globalRightThighOrigin.distance_to(footRightTarget.get_origin()) <= rightLegLength)
                {
                    if (rightStepType != 2)
                    { //means it was already in the process of stepping towards a different target
                        rightStepType = 2;
                    }
                    progressRightStep(centeredRightStep, delta, centerVelocity.length(), heightOffsetRatio);
                }
                else if (globalRightThighOrigin.distance_to(rightPerigee.get_origin()) <= rightLegLength && globalRightThighOrigin.distance_to(footRightTarget.get_origin()) <= rightLegLength)
                {
                    if (rightStepType != 3)
                    { //means it was already in the process of stepping towards a different target
                        rightStepType = 3;
                    }
                    progressRightStep(rightPerigee, delta, centerVelocity.length(), heightOffsetRatio);
                }
                else
                {
                    // printf("hipTarget: %f %f %f\n", hipTarget.get_origin()[0], hipTarget.get_origin()[1], hipTarget.get_origin()[2]);
                    // printf("globalRightThighOrigin: %f %f %f\n", globalRightThighOrigin[0], globalRightThighOrigin[1], globalRightThighOrigin[2]);
                    // printf("rightPerigee: %f %f %f\n", rightPerigee.get_origin()[0], rightPerigee.get_origin()[1], rightPerigee.get_origin()[2]);
                    // printf("distance: %f\n", globalRightThighOrigin.distance_to(rightPerigee.get_origin()));
                    // printf("rightLegLength: %f\n", rightLegLength);
                    if (rightStepType != 4)
                    { //means it was already in the process of stepping towards a different target
                        rightStepType = 4;
                    }
                    dangleRightFoot(rightPerigee);
                }
            }
        }
        footLeftTarget.set_origin(Vector3(footLeftTarget.get_origin()[0], std::max(restFloor, footLeftTarget.get_origin()[1]), footLeftTarget.get_origin()[2]));
        footRightTarget.set_origin(Vector3(footRightTarget.get_origin()[0], std::max(restFloor, footRightTarget.get_origin()[1]), footRightTarget.get_origin()[2]));
    }
}

void HumanIK::dangleLeftFoot(Transform target)
{
    float dangleAngle = safeASIN(relaxAmount / 100); //at 100 its a full pirouette
    float dangleHeight = abs(leftFootLength * sin(dangleAngle));
    target.set_origin(target.get_origin() + Vector3(0, dangleHeight, 0));
    Vector3 cross = target.get_basis()[2].cross(Vector3(0, 1, 0));
    target.rotate_basis(cross.normalized(), dangleAngle);
    float targetDist = (target.get_origin() - footLeftTarget.get_origin()).length();
    footLeftTarget = footLeftTarget.interpolate_with(target, smoothCurve(targetDist, relaxAmount / 10));
    leftPlant = footLeftTarget;
    leftStepping = true;
}

void HumanIK::dangleRightFoot(Transform target)
{
    float dangleAngle = safeASIN(relaxAmount / 100); //at 100 its a full pirouette
    float dangleHeight = abs(leftFootLength * sin(dangleAngle));
    target.set_origin(target.get_origin() + Vector3(0, dangleHeight, 0));
    Vector3 cross = target.get_basis()[2].cross(Vector3(0, 1, 0));
    target.rotate_basis(cross.normalized(), dangleAngle);
    float targetDist = (target.get_origin() - footRightTarget.get_origin()).length();
    footRightTarget = footRightTarget.interpolate_with(target, smoothCurve(targetDist, relaxAmount / 10));
    rightPlant = footRightTarget;
    rightStepping = true;
}

void HumanIK::progressLeftStep(Transform stepTarget, float delta, float centerSpeed, float heightOffsetRatio)
{
    //Raycasting
    // Ref<World> world = skeleton->get_world();
    // if (world.is_valid()) {
    //     RID rid = world->get_space();
    //     PhysicsDirectSpaceState *dss = world->get_direct_space_state();
    //     if (dss) {
    //         Dictionary result = dss->intersect_ray(stepTarget.get_origin() + Vector3(0, rayCastHeight, 0), stepTarget.get_origin() - Vector3(0,rayCastHeight,0), Array(), 0x7FFFFFFF, true, false);
    //         if (result.has("position") && result.has("normal")){
    //             Vector3 position = result["position"];
    //             Vector3 normal = result["normal"];
    //             stepTarget.set_origin(position + normal * stepTarget.get_origin()[1]);

    //             Vector3 cross;
    //             float dot;
    //             crossDot(cross, dot, stepTarget.get_basis()[1], normal);
    //             stepTarget.rotate_basis(cross, safeACOS(dot));
    //         }
    //     }
    // }

    if (leftProgress == 0)
    {
        leftStepTarget = stepTarget;
    }
    leftStepTarget = leftStepTarget.interpolate_with(stepTarget, 0.25);

    leftProgress += delta;
    float adjustedCenterSpeed = centerSpeed / delta;
    float stepDistance = leftPlant.get_origin().distance_to(leftStepTarget.get_origin());
    float stepAngle = leftPlant.get_basis()[2].normalized().dot(leftStepTarget.get_basis()[2].normalized());
    float adjustedStepTime = (stepTime - (0.5 * stepTime * smoothCurve(adjustedCenterSpeed))) * smoothCurve(stepDistance + (1 - stepAngle) * stepTime);
    float adjustedStepHeight = stepHeight * smoothCurve(leftProgress) * powf(1 - heightOffsetRatio, 2) * stepAngle * smoothCurve(adjustedCenterSpeed, 0.1);
    leftPlant.set_origin(leftPlant.get_origin() + hipTarget.get_basis()[1] * adjustedStepHeight);
    if (leftProgress >= adjustedStepTime)
    {
        leftStepping = false;
        footLeftTarget = stepTarget;
        leftPlant = stepTarget;
        leftProgress = 0;
    }
    else if (adjustedStepTime > 0)
    {
        footLeftTarget = leftPlant.interpolate_with(leftStepTarget, leftProgress / adjustedStepTime);
    }
}
void HumanIK::progressRightStep(Transform stepTarget, float delta, float centerSpeed, float heightOffsetRatio)
{
    //Raycasting
    // Ref<World> world = skeleton->get_world();
    // if (world.is_valid()) {
    //     PhysicsDirectSpaceState *dss = world->get_direct_space_state();
    //     if (dss) {
    //         Dictionary result = dss->intersect_ray(stepTarget.get_origin() + Vector3(0, rayCastHeight, 0), stepTarget.get_origin() - Vector3(0,rayCastHeight,0), Array(), 0x7FFFFFFF, true, false);
    //         if (result.has("position") && result.has("normal")){
    //             Vector3 position = result["position"];
    //             Vector3 normal = result["normal"];
    //             stepTarget.set_origin(position + normal * stepTarget.get_origin()[1]);

    //             Vector3 cross;
    //             float dot;
    //             crossDot(cross, dot, stepTarget.get_basis()[1], normal);
    //             stepTarget.rotate_basis(cross, safeACOS(dot));
    //         }
    //     }
    // }

    if (rightProgress == 0)
    {
        rightStepTarget = stepTarget;
    }
    rightStepTarget = rightStepTarget.interpolate_with(stepTarget, 0.25);

    rightProgress += delta;
    float adjustedCenterSpeed = centerSpeed / delta;
    float stepDistance = rightPlant.get_origin().distance_to(rightStepTarget.get_origin());
    float stepAngle = rightPlant.get_basis()[2].normalized().dot(rightStepTarget.get_basis()[2].normalized());
    float adjustedStepTime = (stepTime - (0.5 * stepTime * smoothCurve(adjustedCenterSpeed))) * smoothCurve(stepDistance + (1 - stepAngle) * stepTime);
    float adjustedStepHeight = stepHeight * smoothCurve(rightProgress) * powf(1 - heightOffsetRatio, 2) * stepAngle * smoothCurve(adjustedCenterSpeed, 0.1);
    rightPlant.set_origin(rightPlant.get_origin() + hipTarget.get_basis()[1] * adjustedStepHeight);
    if (rightProgress >= adjustedStepTime)
    {
        rightStepping = false;
        footRightTarget = stepTarget;
        rightPlant = stepTarget;
        rightProgress = 0;
    }
    else if (adjustedStepTime > 0)
    {
        footRightTarget = rightPlant.interpolate_with(rightStepTarget, rightProgress / adjustedStepTime);
    }
}

void HumanIK::setFalling(bool isFalling)
{
    falling = isFalling;
}

void HumanIK::setManualUpdate(bool update)
{
    manualUpdate = update;
}